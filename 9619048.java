/**  
 * 冒泡排序函数  
 * 通过重复地遍历待排序数组，比较并交换相邻元素，使无序数组变得有序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for(int i = 0; i < n - 1; i++) {  
        for(int j = 0; j < n - 1 - i; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if(a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
