/**  
 * 冒泡排序函数  
 * 功能：对数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，表示本趟是否有交换，优化冒泡排序性能  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            if (a[j] > a[j + 1]) {  
                // 交换a[j]和a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，设置标志位为true  
                swapped = true;  
            }  
        }  
        // 如果本趟没有发生交换，说明数组已经有序，直接退出循环  
        if (!swapped) {  
            break;  
        }  
    }  
} // end
