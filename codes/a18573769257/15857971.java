/**  
 * 冒泡排序函数  
 * 将数组a按照从小到大的顺序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，决定冒泡的趟数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，决定每趟比较的次数  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们的位置  
                // 交换a[j]和a[j + 1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
