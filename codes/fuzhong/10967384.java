/**
 * 冒泡排序函数
 * @param arr 待排序的数组
 * @param size 待排序的数组长度
 */
public static void bubbleSort(int [] arr, int size){
    // 你的代码，使无序数组 arr 变得有序
    for(int i = 0 ; i < size - 1 ; i++){
    	for(int j = 0 ; j < size - 1 - i ; j++){
		if(arr[j] > arr[j + 1]){
			int tmp = arr[j];
			arr[j] = arr[j + 1];
			arr[j + 1] = tmp;
		}
	}
    }

} //end

