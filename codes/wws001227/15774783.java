/**  
 * 冒泡排序函数  
 * 通过不断比较相邻元素，并交换位置（如果前一个元素大于后一个元素），将最大元素逐渐“浮”到数组的末尾，  
 * 重复这个过程直到整个数组有序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制冒泡的轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，进行相邻元素的比较和交换  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素  
                // 交换位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
