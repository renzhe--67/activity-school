/**  
 * 冒泡排序函数  
 * 通过多次遍历数组，比较相邻元素并交换位置，将最大的元素逐渐“冒泡”到数组的末尾，  
 * 从而实现数组的升序排序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制遍历次数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环，控制每次遍历的比较次数  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end

