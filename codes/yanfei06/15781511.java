/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for(int end = n - 2; end >= 0; end--)
    {
	    for(int i = 0; i <= end; i++)
	    {
		    if(a[i] > a[i + 1])
		    {
			    int temp = a[i + 1];
			    a[i + 1] = a[i];
			    a[i] = temp;
		    }
	    }
    }

} //end
