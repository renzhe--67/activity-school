/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较并交换位置，使得每一轮循环后最大（或最小）的元素被交换到数组的末尾。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
