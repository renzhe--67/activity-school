/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较并交换，将较大的元素逐渐交换到数组的末尾，达到排序的目的。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制遍历的轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，从前往后比较相邻元素  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
