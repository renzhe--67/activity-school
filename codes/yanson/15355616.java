public static void bubbleSort(int [] a, int n){    
    for (int i = 0; i < n; i++) {
    	for (int j = 0; j < i; j++) {
		if (a[i] <  a[j]) {
		 	int t = a[i];
			a[i] = a[j];
			a[j] = t;
		}
	}
    }
}
